#!/bin/bash


########################################################################
#                                                                      #
#                        M Y S Q L B A C K U P                         #
#                                                                      #
#            BACKUP MYSQL DATABASES AND SEND THEM BY EMAIL             #
#                                                                      #
#                      Copyright 2017 by PB-Soft                       #
#                                                                      #
#                           www.pb-soft.com                            #
#                                                                      #
#                       Version 1.5 / 03.06.2017                       #
#                                                                      #
########################################################################
#
# Check that the following software packages are installed on your
# system if you want to send emails:
#
#  - sendemail (lightweight, command line SMTP email client)
#
#  - libnet-ssleay-perl (perl module to call Secure Sockets Layer (SSL)
#                       functions of the SSLeay library)
#
#  - libio-socket-ssl-perl (perl module that uses SSL to encrypt data)
#
# ======================================================================
#
# You can use the following commands to install the packages:
#
#   apt-get install sendemail
#
#   apt-get install libnet-ssleay-perl
#
#   apt-get install libio-socket-ssl-perl
#
# ======================================================================
#
# Check the following link for information about the sendEmail tool:
#
#   http://caspian.dotconf.net/menu/Software/SendEmail/
#
# ======================================================================
#
# Check that the following two firewall ports are open and only allow
# CIFS (Common Internet File System) file access from the localhost
# (where this script is running) to the external filesystem (NAS inside
# the local LAN network)! Do NOT open the port 445 to the WAN or other
# untrusted networks!
#
#   Port 445 - Common Internet File System (CIFS) => file access
#
#   Port 587 - Simple Mail Transfer Protocol (SMTP) => sending emails
#
# ======================================================================


########################################################################
########################################################################
##############   C O N F I G U R A T I O N   B E G I N   ###############
########################################################################
########################################################################


# ======================================================================
# Specify the backup destination directory.
# ======================================================================
BACKUP_TARGET="/var/www/backup"


# ======================================================================
# Specify the HTTPS link to the backup destination directory.
# ======================================================================
BACKUP_LINK="https://example.com/backup"


# ======================================================================
# Specify how many backups should be stored locally. The backups will
# be stored in positions and the position 1 holds always the newest
# backup. The older the backup the higher the position (number).
# ======================================================================
BACKUP_STORE=10


# ======================================================================
# Specify the connection details for the MySQL host.
# ======================================================================
MYSQL_USERNAME="username"
MYSQL_PASSWORD="password"
MYSQL_HOSTNAME="localhost"


# ======================================================================
# Specify the databases which do NOT have to be saved.
# Use an empty space between the different database names.
# ======================================================================
MYSQL_EXCLUDE="information_schema mysql performance_schema"


# ======================================================================
# Specify if the backup archive should be copied to an external
# location like to a filesystem on a NAS (yes/no).
# ======================================================================
EXT_BACKUP="yes"


# ======================================================================
# Specify the username to access the external filesystem (NAS).
# ======================================================================
EXT_USERNAME="nas_username"


# ======================================================================
# Specify the password to access the external filesystem (NAS).
# ======================================================================
EXT_PASSWORD="nas_password"


# ======================================================================
# Specify IP address of the external backup host (NAS in the LAN).
# ======================================================================
EXT_IP="192.168.10.5"


# ======================================================================
# Specify the external share which has to be mounted (NAS).
# ======================================================================
EXT_SHARE="Backup/myhost"


# ======================================================================
# Specify the mount point for the external backup.
# ======================================================================
EXT_MOUNT_POINT="/mnt/backup"


# ======================================================================
# Specify if email notifications should be sent (yes/no).
# ======================================================================
EMAIL_NOTIFICATIONS="yes"


# ======================================================================
# Specify the connection details for the SMTP host.
# ======================================================================
EMAIL_USERNAME="john.doe@gmail.com"
EMAIL_PASSWORD="smtp_password"
EMAIL_HOSTNAME="smtp.gmail.com"
EMAIL_PORT="587"
EMAIL_TLS="yes"


# ======================================================================
# Specify the email sender.
# ======================================================================
EMAIL_SENDER="admin@example.com"


# ======================================================================
# Specify the email receiver(s). Multiple receivers may be specified by
# separating them by either a white space, comma, or semi-colon like:
#
#  EMAIL_RECEIVER="john@example.com brad@domain.com"
#
# ======================================================================
EMAIL_RECEIVER="john.doe@yahoo.com"


# ======================================================================
# Specify if the backups should be sent by email (yes/no).
# ======================================================================
EMAIL_SEND_BACKUPS="yes"


# ======================================================================
# Specify the maximum size in bytes for the backup files sent by email.
#
#     262144 => 256 KB
#     524288 => 500 KB
#    1048576 => 1 MB
#    2097152 => 2 MB
#    3145728 => 3 MB
#    4194304 => 4 MB
#    5242880 => 5 MB
#   10485760 => 10 MB
#   20971520 => 20 MB
#   52428800 => 50 MB
#
# ======================================================================
EMAIL_MAX_SIZE=2097152


########################################################################
########################################################################
################   C O N F I G U R A T I O N   E N D   #################
########################################################################
########################################################################


# ======================================================================
# Clear the screen.
# ======================================================================
clear


# ======================================================================
# Specify the script start time.
# ======================================================================
SCRIPT_START=`date +%s`


# ======================================================================
# Get the paths of the necessary executables.
# ======================================================================
MYSQL="$(which mysql)"
MYSQLDUMP="$(which mysqldump)"
GZIP="$(which gzip)"
SENDEMAIL="$(which sendEmail)"


# ======================================================================
# Get the actual hostname where the script is running.
# ======================================================================
SCRIPT_HOST="$(hostname)"


# ======================================================================
# Check if the target backup directory does not exist.
# ======================================================================
if [ ! -d $BACKUP_TARGET ]; then


  # ====================================================================
  # Display some information.
  # ====================================================================
  echo
  echo "Create the target backup directory..."


  # ====================================================================
  # Create the target backup directory.
  # ====================================================================
  mkdir -p $BACKUP_TARGET
fi


# ======================================================================
# Save all the existing databases into a variable.
# ======================================================================
DATABASES="$($MYSQL -u $MYSQL_USERNAME -h $MYSQL_HOSTNAME -p$MYSQL_PASSWORD -Bse 'show databases')"


########################################################################
########################################################################
###########        M O U N T   E X T E R N A L   F S        ############
########################################################################
########################################################################


# ======================================================================
# Check if the backup should be copied to an external location.
# ======================================================================
if [ "$EXT_BACKUP" == "yes" ]; then


  # ====================================================================
  # Check if the mountpoint does not exist.
  # ====================================================================
  if [ ! -d "$EXT_MOUNT_POINT" ]; then


    # ==================================================================
    # Create a mountpoint for the external filesystem.
    # ==================================================================
    mkdir -p "$EXT_MOUNT_POINT"
  fi


  # ====================================================================
  # Check if the external filesystem is not already mounted.
  # ====================================================================
  if [ $(mountpoint "$EXT_MOUNT_POINT" | grep "not" | wc -l) == 1 ]; then


    # ==================================================================
    # Display some information.
    # ==================================================================
    echo
    echo "Mount the external filesystem..."


    # ==================================================================
    # Mount the external filesystem.
    # ==================================================================
    sudo mount -t cifs -o username="$EXT_USERNAME",password="$EXT_PASSWORD",uid=1000,gid=1000,iocharset="utf8",rw,dir_mode=0777,file_mode=0666 //"$EXT_IP"/"$EXT_SHARE" "$EXT_MOUNT_POINT"
  fi
fi


########################################################################
########################################################################
#######   L O O P   T H R O U G H   A L L   D A T A B A S E S   ########
########################################################################
########################################################################


# ======================================================================
# Loop through all the existing databases.
# ======================================================================
for db in $DATABASES; do


  ######################################################################
  ######################################################################
  ##########   C H E C K   F O R   E X C L U D E D   D B S   ###########
  ######################################################################
  ######################################################################


  # ====================================================================
  # Set the flag to not backup the databases to -1.
  # ====================================================================
  NO_BACKUP=-1


  # ====================================================================
  # Check if some databases should be excluded from the backup.
  # ====================================================================
  if [ "$MYSQL_EXCLUDE" != "" ]; then


    # ==================================================================
    # Loop through all the excluded databases.
    # ==================================================================
    for i in $MYSQL_EXCLUDE; do


      # ================================================================
      # Check if the actual database has to be excluded.
      # ================================================================
      if [ "$db" == "$i" ]; then


        # ==============================================================
        # Display some information.
        # ==============================================================
        echo
        echo "Exclude database '$db'..."


        # ==============================================================
        # Enable the flag to not backup the database.
        # ==============================================================
        NO_BACKUP=1
      fi
    done
  fi


  ######################################################################
  ######################################################################
  ##########   B A C K U P   M Y S Q L   D A T A B A S E S   ###########
  ######################################################################
  ######################################################################


  # ====================================================================
  # Check if the not backup flag is not set to 1.
  # ====================================================================
  if [ "$NO_BACKUP" == "-1" ]; then


    # ==================================================================
    # Specify the backup start time.
    # ==================================================================
    BACKUP_START=`date +%s`


    # ==================================================================
    # Specify the actual date in the yyyy-mm-dd format. A_DATE for the
    # filename and E_DATE for the email.
    # ==================================================================
    A_DATE="$(date +"%Y-%m-%d")"
    E_DATE="$(date +"%d.%m.%Y")"


    # ==================================================================
    # Specify the actual time in the hh-mm-ss format. A_TIME for the
    # filename and E_TIME for the email.
    # ==================================================================
    A_TIME="$(date +"%H-%M-%S")"
    E_TIME="$(date +"%H:%M:%S")"


    # ==================================================================
    # Specify the backup base name.
    # ==================================================================
    BACKUP_BASENAME="${db}_$SCRIPT_HOST.sql.gz"


    # ==================================================================
    # Specify the backup full name.
    # ==================================================================
    BACKUP_FULLNAME="${A_DATE}_${A_TIME}_$BACKUP_BASENAME"


    # ==================================================================
    # Specify the backup path.
    # ==================================================================
    BACKUP_PATH="$BACKUP_TARGET/$BACKUP_FULLNAME"


    # ==================================================================
    # Display some information.
    # ==================================================================
    echo
    echo "Backup database '$db'..."


    # ==================================================================
    # Backup the database with mysqldump and create a gzip file.
    # ==================================================================
    $MYSQLDUMP -u $MYSQL_USERNAME -h $MYSQL_HOSTNAME -p$MYSQL_PASSWORD --add-drop-table $db | $GZIP -9 > $BACKUP_PATH


    ####################################################################
    ####################################################################
    #########    E X T E R N A L   B A C K U P   ( N A S )    ##########
    ####################################################################
    ####################################################################


    # ==================================================================
    # Check if the external filesystem is mounted.
    # ==================================================================
    if [ ! $(mountpoint "$EXT_MOUNT_POINT" | grep "not" | wc -l) == 1 ]; then


      # ================================================================
      # Check if the new backup archive does exist.
      # ================================================================
      if [ -f "$BACKUP_PATH" ]; then


        # ==============================================================
        # Display some information.
        # ==============================================================
        echo
        echo "Copy database backup '$BACKUP_FULLNAME' to external filesystem..."


        # ==============================================================
        # Copy the actual backup to the external filesystem.
        # ==============================================================
        cp $BACKUP_PATH $EXT_MOUNT_POINT/$BACKUP_FULLNAME
      fi
    fi


    ####################################################################
    ####################################################################
    ############   E - M A I L   N O T I F I C A T I O N   #############
    ####################################################################
    ####################################################################


    # ==================================================================
    # Check if email notifications should be sent.
    # ==================================================================
    if [ "$EMAIL_NOTIFICATIONS" == "yes" ]; then


      # ================================================================
      # Check if the backup file exist.
      # ================================================================
      if [ -f $BACKUP_PATH ]; then


        # ==============================================================
        # Calculate the filesize in bytes.
        # ==============================================================
        FILESIZEB=$(stat -c%s "$BACKUP_PATH")


        # ==============================================================
        # Specify the number/unit.
        # ==============================================================
        NUMBER=1
        UNIT="B"


        # ==============================================================
        # Check if the filesize >= 1 kilobyte.
        # ==============================================================
        if [ "$FILESIZEB" -ge "1024" ]; then


          # ============================================================
          # Specify the number/unit.
          # ============================================================
          NUMBER=1024
          UNIT="KB"


          # ============================================================
          # Check if the filesize >= 1 megabyte.
          # ============================================================
          if [ "$FILESIZEB" -ge "1048576" ]; then


            # ==========================================================
            # Specify the number/unit.
            # ==========================================================
            NUMBER=1048576
            UNIT="MB"
          fi
        fi


        # ==============================================================
        # Calculate the filesize in a readable format.
        # ==============================================================
        DISPLAY_FILE_SIZE=`echo "scale=2;$FILESIZEB / $NUMBER" | bc`


        # ==============================================================
        # Specify the backup end time.
        # ==============================================================
        BACKUP_END=`date +%s`


        # ==============================================================
        # Calculate the backup time.
        # ==============================================================
        BACKUP_TIME=$((BACKUP_END-BACKUP_START))


        # ==============================================================
        # Specify more email details.
        # ==============================================================
        EMAIL_SUBJECT="$E_DATE / $E_TIME | Backup successful: '$BACKUP_FULLNAME'"
        EMAIL_MESSAGE="Start date: $E_DATE\nStart time: $E_TIME\nHostname: $SCRIPT_HOST\nBackup time: $BACKUP_TIME s\nBackup size: $DISPLAY_FILE_SIZE $UNIT\nNewest backup: $BACKUP_LINK/1_$BACKUP_BASENAME"


        # ==============================================================
        # Check if the backup files should be sent by email.
        # ==============================================================
        if [[ "$EMAIL_SEND_BACKUPS" == "yes" && "$FILESIZEB" -le "$EMAIL_MAX_SIZE" ]]; then


          # ============================================================
          # Send a notification and the backup file via email.
          # ============================================================
          $SENDEMAIL -o tls=$EMAIL_TLS -f $EMAIL_SENDER -t $EMAIL_RECEIVER -s $EMAIL_HOSTNAME:$EMAIL_PORT -xu $EMAIL_USERNAME -xp $EMAIL_PASSWORD -u "$EMAIL_SUBJECT" -m "$EMAIL_MESSAGE" -o message-charset=utf-8 -a "$BACKUP_PATH"


        # ==============================================================
        # The backup files should not be sent by email.
        # ==============================================================
        else


          # ============================================================
          # Send only a notification.
          # ============================================================
          $SENDEMAIL -o tls=$EMAIL_TLS -f $EMAIL_SENDER -t $EMAIL_RECEIVER -s $EMAIL_HOSTNAME:$EMAIL_PORT -xu $EMAIL_USERNAME -xp $EMAIL_PASSWORD -u "$EMAIL_SUBJECT" -m "$EMAIL_MESSAGE" -o message-charset=utf-8
        fi


      # ================================================================
      # The backup file does not exist.
      # ================================================================
      else


        # ==============================================================
        # Specify more email details.
        # ==============================================================
        EMAIL_SUBJECT="$E_DATE / $E_TIME | Backup '${db}' on '$SCRIPT_HOST' failed!"
        EMAIL_MESSAGE="$E_DATE / $E_TIME | The backup of the database '${db}' on host '$SCRIPT_HOST' has failed!"


        # ==============================================================
        # Send an error message via email.
        # ==============================================================
        $SENDEMAIL -o tls=$EMAIL_TLS -f $EMAIL_SENDER -t $EMAIL_RECEIVER -s $EMAIL_HOSTNAME:$EMAIL_PORT -xu $EMAIL_USERNAME -xp $EMAIL_PASSWORD -u $EMAIL_SUBJECT -m $EMAIL_MESSAGE -o message-charset=utf-8
      fi
    fi


    ####################################################################
    ####################################################################
    ################   B A C K U P   R O T A T I O N   #################
    ####################################################################
    ####################################################################


    # ==================================================================
    # Lower the new loop counter.
    # ==================================================================
    BACKUP_STORE=`expr $BACKUP_STORE - 1`


    # ==================================================================
    # Loop through all the existing backup files for moving them to a
    # higher position.
    # ==================================================================
    for(( position=$BACKUP_STORE; position>=1; position-- )); do


      # ================================================================
      # Check if in the actual position exist a backup file.
      # ================================================================
      if [ -f $BACKUP_TARGET/${position}_$BACKUP_BASENAME ]; then


        # ==============================================================
        # Specify the next higher position number.
        # ==============================================================
        NEXT=`expr $position + 1`


        # ==============================================================
        # Move the actual backup file to a higher position.
        # ==============================================================
        mv -f $BACKUP_TARGET/${position}_$BACKUP_BASENAME $BACKUP_TARGET/${NEXT}_$BACKUP_BASENAME
      fi
    done


    # ==================================================================
    # Save the actual backup file to the first position.
    # ==================================================================
    mv -f $BACKUP_PATH $BACKUP_TARGET/1_$BACKUP_BASENAME
  fi
done


########################################################################
########################################################################
###########      U N M O U N T   E X T E R N A L   F S      ############
########################################################################
########################################################################


# ======================================================================
# Check if the backup should be copied to an external location.
# ======================================================================
if [ "$EXT_BACKUP" == "yes" ]; then


  # ====================================================================
  # Check if the NAS share is mounted.
  # ====================================================================
  if [ ! $(mountpoint "$EXT_MOUNT_POINT" | grep "not" | wc -l) == 1 ]; then


    # ==================================================================
    # Show the existing external backup files.
    # ==================================================================
    echo
    echo Existing external backups:
    echo --------------------------
    find $EXT_MOUNT_POINT/ -name *_$BACKUP_BASENAME


    # ==================================================================
    # Display some information.
    # ==================================================================
    echo
    echo "Unmount the external filesystem..."


    # ==================================================================
    # Unmount the external filesystem.
    # ==================================================================
    sudo umount $EXT_MOUNT_POINT
  fi
fi


########################################################################
########################################################################
###########   D I S P L A Y   E X E C U T I O N   T I M E   ############
########################################################################
########################################################################


# ======================================================================
# Specify the script ending time.
# ======================================================================
SCRIPT_END=`date +%s`


# ======================================================================
# Calculate the script runtime.
# ======================================================================
SCRIPT_TIME=$((SCRIPT_END-SCRIPT_START))


# ======================================================================
# Display the script runtime.
# ======================================================================
echo "Total execution time: $SCRIPT_TIME seconds"
