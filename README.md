# MySQLBackup Script - README #
---

### Overview ###

The **MySQLBackup** script (Bash) dumps the data of the specified **MySQL Databases** into files, compresses them with **Gzip** and then sends them by email. Now the script can also mount a NAS drive/directory (CIFS) and copy the backup files to this drive/directory.


### Setup ###

* Install the software packages **sendemail**, **libnet-ssleay-perl** and **libio-socket-ssl-perl**.
* You can use the command: **apt-get install sendemail libnet-ssleay-perl libio-socket-ssl-perl**
* Copy the backup script **mysql_backup.sh** to your computer.
* Make the backup script executable: **chmod +x mysql_backup.sh**.
* Edit the configuration part of the **mysql_backup.sh** script.
* Open the two firewall ports needed for file access (**445**) and for sending email (**587**).
* Be sure to NOT open the port **445** to the WAN or another untrusted network!!!!
* Start the backup script from the command prompt: **./mysql_backup.sh**
* Check if the backups are created and sent by email.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **MySQLBackup** script is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](http://opensource.org/licenses/MIT).
